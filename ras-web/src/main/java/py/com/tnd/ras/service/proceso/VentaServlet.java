package py.com.tnd.ras.service.proceso;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import py.com.tdn.ras.business.VentaBusiness;
import py.com.tdn.ras.model.VentaDetalle;

@SuppressWarnings("serial")
@WebServlet("/ventas")
public class VentaServlet extends HttpServlet {

	@EJB
	VentaBusiness ventaBusiness;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		Boolean guardar = Boolean.parseBoolean(request.getParameter("guardar"));
		if (guardar) {
			ventaBusiness.actualizar();
		} else {
			Integer idProducto = Integer.parseInt(request
					.getParameter("idProducto"));
			Integer cantidad = Integer.parseInt(request
					.getParameter("cantidad"));
			Double monto = Double.parseDouble(request.getParameter("monto"));

			VentaDetalle ventaDetalle = new VentaDetalle();
			ventaDetalle.setCantidad(cantidad);
			ventaDetalle.setMonto(monto);
			ventaBusiness.addDetalle(ventaDetalle);

		}

	}
}
