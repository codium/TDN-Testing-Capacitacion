/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.com.tnd.ras.service.proceso;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.com.tdn.ras.business.VentaBusiness;
import py.com.tdn.ras.model.Venta;

import com.fasterxml.jackson.annotation.JsonView;


@Path("/ventas")
public class VentaService {
    @EJB
    VentaBusiness repository;

    @Inject
    private Validator validator;
    
    @JsonView(Venta.Base.class)
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAllOrderedByComprobante() {
    	List<Venta> ventas = null;
    	ventas = repository.findAllOrderedByComprobante();
    	 
        return Response.ok(ventas).build();
    }
  
    @JsonView(Venta.ConDetalle.class)
    @GET
    @Path("/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("id") long id) {
        Venta venta = repository.findById(id);
        
        System.out.println("======= Busca por id");

        
        if (venta == null) {
        	return Response.status(404).build();
        } else {
        	return Response.ok(venta).build();
        }
        
        
    }
    
//    @JsonView(Venta.ConDetalle.class)
//    @GET
//    @Path("/busqueda")
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response ventasEnRangoDeFechas(@QueryParam("descripcion") String descripcion) {
//        Venta venta = repository.ventasEnRangoDeFechas(descripcion);
//        System.out.println("======= Busca por descripcion");
//        if (venta == null) {
//        	return Response.status(404).build();
//        } else {
//        	return Response.ok(venta).build();
//        }
//    }
    
//    @JsonView(Venta.ConDetalle.class)
//    @GET
//    @Path("/busqueda2/{descripcion}")
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response findByDescripcion2(@PathParam("descripcion") String descripcion) {
//        Venta venta = repository.findByDescripcion(descripcion);
//        System.out.println("======= Busca por descripcion dos" + descripcion);
//        if (venta == null) {
//        	return Response.status(404).build();
//        } else {
//        	return Response.ok(venta).build();
//        }
//    }
    
    
    @JsonView(Venta.ConDetalle.class)
    @GET
    @Path("/con-validacion/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    @NotNull(message="El venta no existe")
    public Venta findByIdConValidacion(@PathParam("id") long id) {
        Venta venta = repository.findById(id);

        return venta;
        
    }


    
    @POST
    @Path("/con-validacion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView(Venta.Base.class)
    public Response createVentaConValidacion(@Valid Venta venta) {

       repository.crearVenta(venta);
       
       return Response.ok(venta).build();

    }
    
    
//    @PUT
//    @Path("/{id}")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response actualizarVenta(@PathParam("id") Long id, Venta venta) {
//       repository.actualizarVenta(venta);
//       return Response.ok(venta).build();
//
//    }
//    
//    
//    @DELETE
//    @Path("/{id}")
//    public Response borrarVenta(@PathParam("id") Long ventaId) {
//       repository.eliminarVenta(ventaId);
//       return Response.ok().build();
//    }
//    
//    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView(Venta.Base.class)
    public Response createVenta(Venta venta) {
    	System.out.println("creando venta === ");
    	
        Response.ResponseBuilder builder = null;

        try {
            // Validates venta using bean validation
            validateVenta(venta);
            // Create an "ok" response
            repository.crearVenta(venta);
            builder = Response.ok().entity(venta);
        } catch (ConstraintViolationException ce) {
            // Handle bean validation issues
            builder = createViolationResponse(ce.getConstraintViolations());
        } catch (ValidationException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("email", "Email taken");
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }


    private void validateVenta(Venta venta) throws ConstraintViolationException, ValidationException {
        Set<ConstraintViolation<Venta>> violations = validator.validate(venta);

        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(violations));
        }

    }

    private Response.ResponseBuilder createViolationResponse(Set<ConstraintViolation<?>> violations) {

        Map<String, String> responseObj = new HashMap<>();

        for (ConstraintViolation<?> violation : violations) {
            responseObj.put(violation.getPropertyPath().toString(), violation.getMessage());
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
    }
    
    
    


}
