/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.com.tnd.ras.service.proceso;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.QueryHint;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.com.tdn.ras.business.ProductoBusiness;
import py.com.tdn.ras.model.Producto;

import com.fasterxml.jackson.annotation.JsonView;


@Path("/productos")
public class ProductoService {
    @EJB
    ProductoBusiness repository;

    @Inject
    private Validator validator;
    
    @JsonView(Producto.Base.class)
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listAllProductos() {
    	List<Producto> productos = null;
    	productos = repository.findAllOrderedByDescripcion();
    	 
        return Response.ok(productos).build();
    }
    
    @GET
    @Path("/en-venta/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Producto> listAllProductosEnVenta(@PathParam("id") long id) {
        return repository.findAllProductoEnVenta(id);
    }
    
    @JsonView(Producto.Base.class)
    @GET
    @Path("/nunca-vendidos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Producto> listAllProductosNuncaVendidos() {
        return repository.findAllProductosNuncaVendidos();
    }
    
   
    

    @JsonView(Producto.ConDetalle.class)
    @GET
    @Path("/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("id") long id) {
        Producto producto = repository.findById(id);
        

        //System.out.println(producto);
        
        //if (producto == null) {
        //    throw new WebApplicationException(Response.Status.NOT_FOUND);
        //}
        
        System.out.println("======= Busca por id");

        
        if (producto == null) {
        	return Response.status(404).build();
        } else {
        	return Response.ok(producto).build();
        }
        
        
    }
    
    @JsonView(Producto.ConDetalle.class)
    @GET
    @Path("/busqueda")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findByDescripcion(@QueryParam("descripcion") String descripcion) {
        Producto producto = repository.findByDescripcion(descripcion);
        System.out.println("======= Busca por descripcion");
        if (producto == null) {
        	return Response.status(404).build();
        } else {
        	return Response.ok(producto).build();
        }
    }
    
    @JsonView(Producto.ConDetalle.class)
    @GET
    @Path("/busqueda2/{descripcion}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findByDescripcion2(@PathParam("descripcion") String descripcion) {
        Producto producto = repository.findByDescripcion(descripcion);
        System.out.println("======= Busca por descripcion dos" + descripcion);
        if (producto == null) {
        	return Response.status(404).build();
        } else {
        	return Response.ok(producto).build();
        }
    }
    
    
    @JsonView(Producto.ConDetalle.class)
    @GET
    @Path("/con-validacion/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    @NotNull(message="El producto no existe")
    public Producto findByIdConValidacion(@PathParam("id") long id) {
        Producto producto = repository.findById(id);

        return producto;
        
    }


    
    @POST
    @Path("/con-validacion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView(Producto.Base.class)
    public Response createProductoConValidacion(@Valid Producto producto) {

       repository.crearProducto(producto);
       
       return Response.ok(producto).build();

    }
    
    
    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarProducto(@PathParam("id") Long id, Producto producto) {

       repository.actualizarProducto(producto);
       
       return Response.ok(producto).build();

    }
    
    
    @DELETE
    @Path("/{id}")
    public Response borrarProducto(@PathParam("id") Long produtoId) {
       repository.eliminarProducto(produtoId);
       return Response.ok().build();
    }
    
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView(Producto.Base.class)
    public Response createProducto(Producto producto) {

        Response.ResponseBuilder builder = null;

        try {
            // Validates producto using bean validation
            validateProducto(producto);
            // Create an "ok" response
            repository.crearProducto(producto);
            builder = Response.ok().entity(producto);
        } catch (ConstraintViolationException ce) {
            // Handle bean validation issues
            builder = createViolationResponse(ce.getConstraintViolations());
        } catch (ValidationException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("email", "Email taken");
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }


    private void validateProducto(Producto producto) throws ConstraintViolationException, ValidationException {
        Set<ConstraintViolation<Producto>> violations = validator.validate(producto);

        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(violations));
        }

    }

    private Response.ResponseBuilder createViolationResponse(Set<ConstraintViolation<?>> violations) {

        Map<String, String> responseObj = new HashMap<>();

        for (ConstraintViolation<?> violation : violations) {
            responseObj.put(violation.getPropertyPath().toString(), violation.getMessage());
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
    }
    
    
    


}
