package py.com.tnd.ras.service.proceso;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.com.tdn.ras.dao.ProcesoDAO;
import py.com.tdn.ras.model.Proceso;

/**
 * Created by gaonag on 02/01/15.
 */
@Path("/proceso")
@RequestScoped
public class ProcesoService {

	@EJB
	ProcesoDAO dao;


	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenerRoutingNumber() throws Exception {
		Proceso proceso = dao.getByDescripcion("hola");
		// System.out.println("Proceso: " + proceso.getDescripcion());
		return Response.ok().entity(proceso).build();
	}

}
