package py.com.tdn.ras.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "Dali", date = "2015-03-14T09:38:33.338-0300")
@StaticMetamodel(VentaDetalle.class)
public class VentaDetalle_ {
	public static volatile SingularAttribute<VentaDetalle, Long> id;
	public static volatile SingularAttribute<VentaDetalle, Integer> cantidad;
	public static volatile SingularAttribute<VentaDetalle, Double> monto;
	public static volatile SingularAttribute<VentaDetalle, Venta> venta;
	public static volatile SingularAttribute<VentaDetalle, Producto> producto;
}
