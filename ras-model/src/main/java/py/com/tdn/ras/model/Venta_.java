package py.com.tdn.ras.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "Dali", date = "2015-03-14T09:38:33.334-0300")
@StaticMetamodel(Venta.class)
public class Venta_ {
	public static volatile SingularAttribute<Venta, Long> id;
	public static volatile SingularAttribute<Venta, String> nroComprobante;
	public static volatile SingularAttribute<Venta, Double> montoTotal;
	public static volatile ListAttribute<Venta, VentaDetalle> ventaDetalles;
	public static volatile SingularAttribute<Venta, Cliente> cliente;
}
