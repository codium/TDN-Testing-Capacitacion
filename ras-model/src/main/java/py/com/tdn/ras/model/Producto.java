/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.com.tdn.ras.model;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@Entity
@Table(name = "producto")
public class Producto implements Serializable {
    private static final long serialVersionUID = 1L;
    
    public static class Base {}
    public static class ConDetalle extends Base {}
    
    @JsonView(Producto.Base.class)
    @Id
    @GeneratedValue
    private Long id;

    @JsonView({Producto.Base.class, Venta.ConDetalle.class})
    @NotNull(message="{NOT_NULL}")
    @Size(min = 3, max = 255, message = "1-255 letters and spaces")
    private String descripcion;

    @JsonView(Producto.ConDetalle.class)
    @OneToMany
    @JoinColumn(name = "producto_id")
    private List<VentaDetalle> ventaDetalles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
	public List<VentaDetalle> getVentaDetalles() {
		return ventaDetalles;
	}

	public void setVentaDetalles(List<VentaDetalle> ventaDetalles) {
		this.ventaDetalles = ventaDetalles;
	}
	
	
	public static Producto valueOf(String json) {
		ObjectMapper mapper = new ObjectMapper();
		Producto producto = null;

		try {
			producto = mapper.readValue(json, Producto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return producto;
	}

	public String toString() {
		ObjectMapper mapper = new ObjectMapper();
		StringWriter json = new StringWriter();

		try {
			mapper.writeValue(json, this);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json.toString();
	}
	
	
	public String toStringWithView(Class<?> view) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectWriter w = mapper.writerWithView(view);
		StringWriter json = new StringWriter();

		try {
			w.writeValue(json, this);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json.toString();
	}

}
