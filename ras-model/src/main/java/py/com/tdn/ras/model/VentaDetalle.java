/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.com.tdn.ras.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "venta_detalle")
public class VentaDetalle implements Serializable {
    private static final long serialVersionUID = 1L;

    public static class Base {}
    public static class ConDetalle extends Base {}
    
    @Id
    @GeneratedValue
    @JsonView(VentaDetalle.Base.class)
    private Long id;

    @NotNull
    @Column(name="cantidad")
    @JsonView({VentaDetalle.Base.class, Producto.Base.class, Venta.ConDetalle.class})
    private Integer cantidad;
    
    @NotNull
    @Column(name="monto")
    @JsonView({VentaDetalle.Base.class, Producto.Base.class, Venta.ConDetalle.class})
    private Double monto;
    
    @ManyToOne
    @JsonView(VentaDetalle.ConDetalle.class)
    @JoinColumn(name="venta_id")
    private Venta venta;
    
    @ManyToOne
    @JoinColumn(name="producto_id")
    @JsonView({VentaDetalle.ConDetalle.class, Venta.ConDetalle.class})
    private Producto producto;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public Venta getVenta() {
		return venta;
	}

	public void setVenta(Venta venta) {
		this.venta = venta;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	
	

}
