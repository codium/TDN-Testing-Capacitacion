package py.com.tdn.ras.model;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Proceso
 *
 */
@Entity
@Table(name = "procesos")
public class Proceso implements Serializable {

	
	private static final long serialVersionUID = 1L;
	

	@Column(name = "CODIGO_PROCESO", length = 5)
	@Id
	private Integer codigoProceso;
	
	@Column(name = "DESCRIPCION", length = 35)
	private String descripcion;
	
	

	public Proceso() {
		super();
	}
	
	
	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Integer getCodigoProceso() {
		return codigoProceso;
	}

	public void setCodigoProceso(Integer codigoProceso) {
		this.codigoProceso = codigoProceso;
	}
   
}
