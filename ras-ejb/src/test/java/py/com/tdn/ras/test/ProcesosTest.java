package py.com.tdn.ras.test;

import java.util.ArrayList;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import py.com.tdn.ras.business.ProductoBusiness;
import py.com.tdn.ras.business.impl.ProductoBusinessImpl;
import py.com.tdn.ras.dao.ProductoDAO;
import py.com.tdn.ras.dao.Prueba;
import py.com.tdn.ras.dao.impl.ProductoDAOImpl;
import py.com.tdn.ras.exception.BusinessException;
import py.com.tdn.ras.model.Producto;

@RunWith(Arquillian.class)
public class ProcesosTest {

	@EJB
	ProductoDAO productoDao;
	@Inject
	Producto producto;

	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap
				.create(JavaArchive.class)
				.addClass(Prueba.class)
				.addPackage(ProductoDAO.class.getPackage())
				.addPackage(ProductoDAOImpl.class.getPackage())
				.addPackage(BusinessException.class.getPackage())
				.addPackage(ProductoBusinessImpl.class.getPackage())
				.addPackage(ProductoBusiness.class.getPackage())
				.addPackage(Producto.class.getPackage())
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
				.addAsManifestResource("persistence.xml",
						ArchivePaths.create("persistence.xml"));
		// .addAsResource("persistence.xml", "META-IN/persistence.xml");
	}

	@Test
	public void eliminarTodos() {
		System.out.println("TEST ELIMINAR PRODUCTO");
		ArrayList<Producto> productos = (ArrayList<Producto>) productoDao
				.findAllOrderedByDescripcion();
		for (Producto p : productos) {
			System.out.println("Eliminando producto: " + p.getId().toString());
			productoDao.eliminarProducto(p.getId());
		}
	}

	

	@Test
	public void guardarProducto() {
		System.out.println("TEST GUARDAR PRODUCTO");
		System.out.println("Agregando mi primer producto");
		// producto = new Producto();
		producto.setDescripcion("Mi primer producto");
		System.out.println("Persistiendo");
		productoDao.crearProducto(producto);
		System.out.println("Fin de la prueba");
	}
	
	@Test
	public void existeProducto() {
		System.out.println("TEST EXISTE PRODUCTO");
		System.out.println("Buscando");
		producto = productoDao.getByDescripcion("Mi primer producto");
		Assert.assertEquals("Mi primer producto", producto.getDescripcion());
	}

}
