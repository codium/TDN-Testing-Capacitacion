package py.com.tdn.ras.dao;

import javax.ejb.Local;

import py.com.tdn.ras.model.Proceso;

@Local
public interface ProcesoDAO {
	
	Proceso getByDescripcion(String descripcion);

}
