package py.com.tdn.ras.dao;

import javax.ejb.Local;

import py.com.tdn.ras.model.VentaDetalle;

@Local
public interface VentaDetalleDAO extends BasicDAO<VentaDetalle, Long> {
	

}
