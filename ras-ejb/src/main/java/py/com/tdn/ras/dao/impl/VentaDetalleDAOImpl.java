package py.com.tdn.ras.dao.impl;

import javax.ejb.Stateless;

import py.com.tdn.ras.dao.VentaDetalleDAO;
import py.com.tdn.ras.model.VentaDetalle;

@Stateless
public class VentaDetalleDAOImpl extends BasicDAOImpl<VentaDetalle, Long> implements VentaDetalleDAO {

}
