package py.com.tdn.ras.dao.impl;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import py.com.tdn.ras.dao.VentaDAO;
import py.com.tdn.ras.exception.Utility;
import py.com.tdn.ras.model.Venta;
import py.com.tdn.ras.model.Venta_;

@Stateless
public class VentaDAOImpl extends BasicDAOImpl<Venta, Long> implements VentaDAO {

	
	@PersistenceContext(unitName = "ras-model")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public VentaDAOImpl() {
		// TODO Auto-generated constructor stub
	}
	
	@Inject
	Utility u;

	@Override
	public void printUtility() {
		System.out.println(u);

	}

	@Override
	public Venta crearVenta(Venta venta) {
		return persist(venta);
	}

	@Override
	public void eliminarVenta(Long id) {
		remove(find(id));
	}

	@Override
	public void actualizarVenta(Venta venta) {
		merge(venta);
	}

	@Override
	public Venta findById(Long id) {
		return find(id);
	}

	@Override
	public List<Venta> ventasEnRangoDeFechas(Date fechaDesde, Date fechaHasta) {
		// TODO Auto-generated method stub
		return null;
	}
	


	@Override
	public List<Venta> findAllOrderedByComprobante() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Venta> criteria = cb.createQuery(Venta.class);
        Root<Venta> member = criteria.from(Venta.class);
        criteria.select(member).orderBy(cb.asc(member.get(Venta_.nroComprobante)));
        
        return em.createQuery(criteria).getResultList();
	}

}
