package py.com.tdn.ras.dao;

import java.util.List;

import javax.ejb.Local;

import py.com.tdn.ras.model.Producto;

@Local
public interface ProductoDAO extends BasicDAO<Producto, Long> {
	
	public Producto getByDescripcion(String descripcion);

	public void crearProducto(Producto producto);
	
	public void eliminarProducto(Long id);
	
	public void actualizarProducto(Producto producto);
	
	public Producto findById(Long id);
	
	public Producto findByDescripcion(String descripcion);
	
	public List<Producto> findAllOrderedByDescripcion();
	
	public List<Producto> findAllProductoEnVenta(Long ventaId);
	
	public List<Producto> findAllProductosNuncaVendidos();

}
