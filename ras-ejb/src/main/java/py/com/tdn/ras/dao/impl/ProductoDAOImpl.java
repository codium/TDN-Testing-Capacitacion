package py.com.tdn.ras.dao.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import py.com.tdn.ras.dao.ProductoDAO;
import py.com.tdn.ras.model.Producto;
import py.com.tdn.ras.model.Producto_;
import py.com.tdn.ras.model.Venta;
import py.com.tdn.ras.model.VentaDetalle;
import py.com.tdn.ras.model.VentaDetalle_;
import py.com.tdn.ras.model.Venta_;

/**
 * Session Bean implementation class ProcesoDAOImpl
 */
@Stateless
public class ProductoDAOImpl extends BasicDAOImpl<Producto, Long> implements
		ProductoDAO {

	@PersistenceContext(unitName = "ras-model")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public ProductoDAOImpl() {
		// TODO Auto-generated constructor stub
	}

	
    public Producto findById(Long id) {
    	Producto p = em.find(Producto.class, id);
        return p;
    }

    @Override
    public Producto getByDescripcion(String descripcion) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Producto> criteria = cb.createQuery(Producto.class);
        Root<Producto> Producto = criteria.from(Producto.class);
        criteria.select(Producto).where(cb.equal(Producto.get(Producto_.descripcion), descripcion));
        Producto result = null;
        try {
            result = em.createQuery(criteria).getSingleResult();
        } catch (NoResultException e) {
            System.out.println("sin resultados");
        }
        return result;
    }

    public List<Producto> findAllOrderedByDescripcion() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Producto> criteria = cb.createQuery(Producto.class);
        Root<Producto> member = criteria.from(Producto.class);
        criteria.select(member).orderBy(cb.asc(member.get(Producto_.descripcion)));
        
        return em.createQuery(criteria).getResultList();
    }
    
    //Ejercicio 3 - Liste todos los productos de una venta.
    public List<Producto> findAllProductoEnVenta(Long ventaId) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Producto> criteria = cb.createQuery(Producto.class);
        Root<Producto> member = criteria.from(Producto.class);
        Join<VentaDetalle, Venta> ventas = member.join(Producto_.ventaDetalles).join(VentaDetalle_.venta);
        criteria.select(member).where(cb.equal(ventas.get(Venta_.id), ventaId));
        
        /*
        String query = "select p from Producto p join p.ventaDetalles vd where vd.venta.id = :ventaId";
        TypedQuery<Producto> typedQuery = em.createQuery(query, Producto.class);
        typedQuery.setParameter("ventaId", ventaId);
        
        return typedQuery.getResultList();
        */
        return em.createQuery(criteria).getResultList();
    }
    
    //Ejercicio 4 - Liste todos los productos que nunca fueron vendidos.
    public List<Producto> findAllProductosNuncaVendidos() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Producto> criteria = cb.createQuery(Producto.class);
        Root<Producto> member = criteria.from(Producto.class);
        
        criteria.select(member).where(cb.isEmpty(member.get(Producto_.ventaDetalles)));
        
        //String query = "select p from Producto p where p.ventaDetalles is empty";
        
        return em.createQuery(criteria).getResultList();
        //return em.createQuery(query).getResultList();
    }


	@Override
	public void crearProducto(Producto producto) {
		em.persist(producto);
	}


	@Override
	public void eliminarProducto(Long id) {
		remove(find(id));
	}


	@Override
	public void actualizarProducto(Producto producto) {
		em.merge(producto);
	}


	@Override
	public Producto findByDescripcion(String descripcion) {
		return getByDescripcion(descripcion);
	}
    
    
}
