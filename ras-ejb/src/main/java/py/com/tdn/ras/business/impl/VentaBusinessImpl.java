package py.com.tdn.ras.business.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.TransactionSynchronizationRegistry;

import py.com.tdn.ras.business.VentaBusiness;
import py.com.tdn.ras.business.VentaDetalleBusiness;
import py.com.tdn.ras.dao.VentaDAO;
import py.com.tdn.ras.model.Venta;
import py.com.tdn.ras.model.VentaDetalle;

@Stateless
public class VentaBusinessImpl implements VentaBusiness {

	@PersistenceContext(unitName = "ras-model")
	private EntityManager entityManager;

	@Resource
	TransactionSynchronizationRegistry txReg;

	Venta venta;

	@EJB
	VentaDAO ventaDao;
	
	@EJB
	VentaDetalleBusiness ventaDetalleBusiness;

	@PostConstruct
	public void initVenta() {

		ventaDao.printUtility();
		this.venta = entityManager.find(Venta.class, new Long(1));
		try {
			ventaDao.detachEntity(venta);
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println(entityManager.contains(venta));
	}

	@Override
	public void addDetalle(VentaDetalle detalle) {

		if (venta.getVentaDetalles() == null) {
			venta.setVentaDetalles(new ArrayList<VentaDetalle>());
		}
		detalle.setVenta(this.venta);
		venta.getVentaDetalles().add(detalle);
	}

	@Override
	public void actualizar() {
		ventaDao.merge(venta);
	}

	@Override
	public Venta crearVenta(Venta venta) {
		return ventaDao.crearVenta(venta);
	}

	@Override
	public List<Venta> ventasEnRangoDeFechas(Date fechaDesde, Date fechaHasta) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Venta findById(Long id) {
		Venta venta = ventaDao.findById(id);
		
		try {
			List<VentaDetalle> detalles = ventaDetalleBusiness.detachEntities(venta.getVentaDetalles());
			ventaDao.detachEntity(venta);
			venta.setVentaDetalles(detalles);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		return venta;
	}

	@Override
	public List<Venta> findAllOrderedByComprobante() {
		
		return ventaDao.findAllOrderedByComprobante();
	}
	
	
	

}
