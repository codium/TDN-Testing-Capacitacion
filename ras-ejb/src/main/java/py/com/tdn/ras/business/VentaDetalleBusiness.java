package py.com.tdn.ras.business;

import java.util.List;

import javax.ejb.Local;

import py.com.tdn.ras.model.VentaDetalle;

@Local
public interface VentaDetalleBusiness {

	public VentaDetalle detachEntity(VentaDetalle entity) throws Exception;

	public List<VentaDetalle> detachEntities(List<VentaDetalle> entities) throws Exception;
}
