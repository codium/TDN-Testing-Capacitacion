package py.com.tdn.ras.business;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import py.com.tdn.ras.model.Venta;
import py.com.tdn.ras.model.VentaDetalle;

@Local
public interface VentaBusiness {

	public void addDetalle(VentaDetalle detalle);

	public void actualizar();
	
	public Venta crearVenta(Venta venta);
	
	public List<Venta> ventasEnRangoDeFechas(Date fechaDesde, Date fechaHasta);
	
	public Venta findById(Long ventaId);
	
	public List<Venta> findAllOrderedByComprobante();

}
