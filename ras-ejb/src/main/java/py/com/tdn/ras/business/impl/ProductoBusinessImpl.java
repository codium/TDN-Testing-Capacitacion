package py.com.tdn.ras.business.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.TransactionSynchronizationRegistry;

import py.com.tdn.ras.business.ProductoBusiness;
import py.com.tdn.ras.business.VentaDetalleBusiness;
import py.com.tdn.ras.dao.ProductoDAO;
import py.com.tdn.ras.model.Producto;

@Stateless
public class ProductoBusinessImpl implements ProductoBusiness {

	@PersistenceContext(unitName = "ras-model")
	private EntityManager entityManager;

	@Resource
	TransactionSynchronizationRegistry txReg;

	@EJB
	ProductoDAO productoDao;
	
	@EJB
	VentaDetalleBusiness ventaDetalleBusiness;

	@Override
	public void getByDescripcion(String descripcion) {
		productoDao.getByDescripcion(descripcion);
	}

	@Override
	public void crearProducto(Producto producto) {
		productoDao.crearProducto(producto);
	}

	@Override
	public void eliminarProducto(Long id) {
		productoDao.eliminarProducto(id);
	}

	@Override
	public void actualizarProducto(Producto producto) {
		productoDao.actualizarProducto(producto);
	}

	@Override
	public Producto findById(Long id) {
		Producto producto = productoDao.findById(id);
		
		try {
			ventaDetalleBusiness.detachEntities(producto.getVentaDetalles());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return producto;
	}

	@Override
	public Producto findByDescripcion(String descripcion) {
		Producto producto = productoDao.findByDescripcion(descripcion);
		
		try {
			ventaDetalleBusiness.detachEntities(producto.getVentaDetalles());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return producto;
	}

	@Override
	public List<Producto> findAllOrderedByDescripcion() {
		return productoDao.findAllOrderedByDescripcion();
	}
	
	@Override
	public List<Producto> findAllProductoEnVenta(Long ventaId) {
		return productoDao.findAllProductoEnVenta(ventaId);
	}

	@Override
	public List<Producto> findAllProductosNuncaVendidos() {
		return productoDao.findAllProductosNuncaVendidos();
	}

}
