package py.com.tdn.ras.dao;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import py.com.tdn.ras.model.Venta;

@Local
public interface VentaDAO extends BasicDAO<Venta, Long> {

	public void printUtility();
	
	public Venta crearVenta(Venta venta);
	
	public void eliminarVenta(Long id);
	
	public void actualizarVenta(Venta venta);
	
	public Venta findById(Long id);
	
	public List<Venta> ventasEnRangoDeFechas(Date fechaDesde, Date fechaHasta);
	
	public List<Venta> findAllOrderedByComprobante();
	

}
