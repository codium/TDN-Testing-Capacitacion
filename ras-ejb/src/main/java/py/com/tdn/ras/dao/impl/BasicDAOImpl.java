package py.com.tdn.ras.dao.impl;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PersistenceContext;

import py.com.tdn.ras.dao.BasicDAO;

public class BasicDAOImpl<T, PK extends Serializable> implements
		BasicDAO<T, PK> {

	protected Class<T> entityClass;

	@PersistenceContext(unitName = "ras-model")
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public BasicDAOImpl() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass()
				.getGenericSuperclass();
		this.entityClass = (Class<T>) genericSuperclass
				.getActualTypeArguments()[0];
	}

	@Override
	public T detachEntity(T entity) throws Exception {

		entityManager.detach(entity);
		for (Field field : entityClass.getDeclaredFields()) {
			field.setAccessible(true);
			if (field.isAnnotationPresent(OneToMany.class)
					|| field.isAnnotationPresent(ManyToMany.class)) {
				field.set(entity, null);
			}
			if (field.isAnnotationPresent(OneToOne.class)
					|| field.isAnnotationPresent(ManyToOne.class)) {
				entityManager.detach(field.get(entity));
			}
		}
		return entity;
	}

	@Override
	public List<T> detachEntities(List<T> entities) throws Exception {
		//List<T> detachedEntities = new ArrayList<T>(); 
		if (entities != null) {
			for (T entidad : entities) {
				detachEntity(entidad);
			}
		}
		return entities;
	}

	@Override
	public T persist(T t) {
		entityManager.persist(t);
		return t;
	}

	@Override
	public T merge(T t) {
		t = entityManager.merge(t);
		return t;
	}

	@Override
	public void remove(T t) {
		entityManager.remove(t);

	}

	@Override
	public void truncate() {
		// TODO Auto-generated method stub
	}
	
	public T find(final PK id) {
		return entityManager.find(entityClass, id);
	}

}
