package py.com.tdn.ras.business.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import py.com.tdn.ras.business.VentaDetalleBusiness;
import py.com.tdn.ras.dao.VentaDetalleDAO;
import py.com.tdn.ras.model.VentaDetalle;

@Stateless
public class VentaDetalleBusinessImpl implements VentaDetalleBusiness {

	@PersistenceContext(unitName = "ras-model")
	private EntityManager entityManager;


	@EJB
	VentaDetalleDAO ventaDetalleDao;


	@Override
	public VentaDetalle detachEntity(VentaDetalle entity) throws Exception {
		return ventaDetalleDao.detachEntity(entity);
	}


	@Override
	public List<VentaDetalle> detachEntities(List<VentaDetalle> entities)
			throws Exception {
		return ventaDetalleDao.detachEntities(entities);
	}

	
}
