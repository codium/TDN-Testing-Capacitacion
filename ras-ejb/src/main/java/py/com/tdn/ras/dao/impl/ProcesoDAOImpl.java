package py.com.tdn.ras.dao.impl;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import py.com.tdn.ras.dao.ProcesoDAO;
import py.com.tdn.ras.model.Proceso;

/**
 * Session Bean implementation class ProcesoDAOImpl
 */
@Stateless
@LocalBean
public class ProcesoDAOImpl implements
		ProcesoDAO {

	@PersistenceContext(unitName = "ras-model")
	private EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public ProcesoDAOImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Proceso getByDescripcion(String descripcion) {
		Proceso proceso = null;
		System.out.println("llamada a GET BY DESCRIPCON:__" + descripcion + "___");
		try {
			proceso = (Proceso) this.entityManager
					.createQuery(
							"Select c from Proceso c where c.descripcion like :descripcion")
					.setParameter("descripcion", descripcion.trim())
					.getSingleResult();
		} catch (Exception nre) {
			nre.printStackTrace();
		}
		return proceso;
	}
}
